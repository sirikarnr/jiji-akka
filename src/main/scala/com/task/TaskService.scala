package com.task

import java.util.UUID

import com.task.TaskActor.ActionPerformedTask
import com.task.database.{ MongoCRUD, MongoDbSetup }
import spray.json.{ JsonParser, RootJsonFormat }

class TaskService extends JsonSupport {
  val repo: MongoCRUD = new MongoCRUD(MongoDbSetup.mongoDB("task"))

  def list: Tasks = {
    repo.findAll match {
      case Left(err) => Tasks(None)
      case Right(data) =>
        val datas = data.map(t => convertToTask(t))
        Tasks(Some(datas))
    }
  }

  def create(task: Task): Response = {
    val randomId = UUID.randomUUID().toString
    val newTask: Task = Task(Some(randomId), task.title, task.deadLine, task.detail, task.status match {
      case Some(stat) => Some(stat)
      case _ => Some("active")
    })
    repo.insert(newTask) match {
      case Left(err) => Response(err.message, err.code)
      case Right(data) => Response(data, Status.SUCCESS)
    }
  }

  def find(id: String): Response = {
    repo.find(id) match {
      case Left(err) => Response(err.message, err.code)
      case Right(task) => Response(convertToTask(task), Status.SUCCESS)
    }
  }

  def delete(id: String): Response = {
    repo.delete(id) match {
      case Left(err) => Response(err.message, err.code)
      case Right(msg) => Response(msg, Status.SUCCESS)
    }
  }

  def convertToTask(jsonString: String): Task = {
    JsonParser(jsonString).convertTo[Task]
  }

}

package com.task

import akka.actor.{ Actor, ActorLogging, Props }

final case class Task(id: Option[String], title: String, deadLine: String, detail: String, status: Option[String])

final case class Response(data: Any, code: Int)

final case class Tasks(data: Option[List[Task]], code: Int = 200)

object TaskActor {
  final case class ActionPerformedTask(data: Task, code: Int = 200)
  final case class ActionPerformedTaskInfo(data: String, code: Int = 200)
  final case class ActionPerformedTaskData(data: Tasks, code: Int = 200)
  final case object GetTasks
  final case class CreateTask(task: Task)
  final case class GetTask(id: String)
  final case class DeleteTask(id: String)

  def props: Props = Props[TaskActor]
}
class TaskActor extends Actor with ActorLogging {
  import TaskActor._
  var tasks = Set.empty[Task]
  implicit val service: TaskService = new TaskService

  def receive: Receive = {
    case GetTasks => sender() ! service.list
    case CreateTask(task) => sender() ! service.create(task)
    case GetTask(id) => sender() ! service.find(id)
    case DeleteTask(id) => sender ! service.delete(id)
  }
}

case object Status {
  final val CREATED: Int = 201
  final val SUCCESS: Int = 200
  final val NOT_FOUND = 404
  final val INTERNAL_SERVER_ERROR = 500
}

package com.task

import akka.actor.{ ActorRef, ActorSystem }
import akka.http.scaladsl.marshalling.ToResponseMarshallable
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.MethodDirectives.{ delete, get, post }
import akka.http.scaladsl.server.directives.PathDirectives.path
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import akka.pattern.ask
import akka.util.Timeout
import TaskActor._

import scala.concurrent.Future
import scala.concurrent.duration._

trait AppRoutes extends JsonSupport {

  implicit def system: ActorSystem
  def taskActor: ActorRef
  implicit val timeout = Timeout(5.seconds)

  val routes: Route =
    pathPrefix("tasks") {
      concat(
        pathEnd {
          concat(
            get {
              val tasks: Future[Tasks] =
                (taskActor ? GetTasks).mapTo[Tasks]
              complete(tasks)
            },
            post {
              entity(as[Task]) { task =>
                val taskCreated: Future[Response] =
                  (taskActor ? CreateTask(task)).mapTo[Response]
                onSuccess(taskCreated) { performed => complete(performed)
                }
              }
            }
          )
        },
        path(Segment) {
          id =>
            concat(
              get {
                val task: Future[Response] =
                  (taskActor ? GetTask(id)).mapTo[Response]
                complete(task)
              },

              delete {
                val taskDeleted: Future[Response] =
                  (taskActor ? DeleteTask(id)).mapTo[Response]
                onSuccess(taskDeleted) { performed =>
                  complete(performed)
                }

              }
            )
        }
      )
    }
}

package com.task

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import com.task.TaskActor.{ ActionPerformedTask, ActionPerformedTaskData }
import spray.json.{ JsObject, JsString, JsValue, JsonFormat }

trait JsonSupport extends SprayJsonSupport {
  import spray.json.DefaultJsonProtocol._

  implicit val responseJsonFormat = jsonFormat2(Response)
  implicit val taskJsonFormat = jsonFormat5(Task)
  implicit val actionPerformedTaskJsonFormat = jsonFormat2(ActionPerformedTask)
  implicit val tasksJsonFormat = jsonFormat2(Tasks)
  implicit val actionPerformedTaskData = jsonFormat2(ActionPerformedTaskData)

  implicit object AnyJsonFormat extends JsonFormat[Any] {
    override def write(obj: Any): JsValue = obj match {
      case message: String => JsString(message)
      case _ => JsString("")
    }

    override def read(json: JsValue): Any = json match {
      case JsString(s) => s
      case _ => None
    }
  }

}


package com.task.database

import java.util.UUID

import com.mongodb.casbah.Imports._
import com.mongodb.casbah.commons.MongoDBObject
import com.task.{ CustomExcecption, DataNotFoundException, DatabaseException, Task }

import scala.util.{ Failure, Success, Try }

class MongoCRUD(db: MongoCollection) {
  def findAll: Either[CustomExcecption, List[String]] = {
    Try(db.find.toSeq) match {
      case Success(data) => Right(data.toList.map(task => task.toString))
      case Failure(err) => Left(DatabaseException)
    }
  }

  def find(id: String): Either[CustomExcecption, String] = {
    val query = MongoDBObject("id" -> id)
    Try(db.findOne(query)) match {
      case Success(None) => Left(DataNotFoundException)
      case Success(Some(task)) => Right(task.toString)
      case Failure(err) => Left(DatabaseException)
    }
  }

  def insert(task: Task): Either[CustomExcecption, Task] = {
    Try(db.insert(buildObject(task))) match {
      case Success(data) => Right(task)
      case Failure(error) => Left(DatabaseException)
    }
  }

  def buildObject(task: Task): MongoDBObject = {
    val builder = MongoDBObject.newBuilder
    builder += "id" -> UUID.randomUUID().toString
    builder += "title" -> task.title
    builder += "deadLine" -> task.deadLine
    builder += "detail" -> task.detail
    builder += "status" -> task.status
    builder.result()
  }

  def delete(id: String): Either[CustomExcecption, _] = {
    val query = MongoDBObject("id" -> id)
    val res = db.findAndRemove(query)
    Try(res) match {
      case Failure(err) => Left(DatabaseException)
      case Success(None) => Left(DataNotFoundException)
      case Success(task) => Right("success")
    }
  }
}

package com.task

import akka.actor.{ ActorRef, ActorSystem }
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer

import scala.concurrent.Await
import scala.concurrent.duration.Duration

object AppServer extends App with AppRoutes {
  implicit val system: ActorSystem = ActorSystem("todoapi")
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  val taskActor: ActorRef = system.actorOf(TaskActor.props, "taskActor")

  val apiRoutes: Route = routes

  Http().bindAndHandle(apiRoutes, "localhost", 8000)
  println(s"server online at http://localost:8000/")
  Await.result(system.whenTerminated, Duration.Inf)
}

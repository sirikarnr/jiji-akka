package com.task

trait CustomExcecption {
  val message: String = "process failed"
  val code = Status.INTERNAL_SERVER_ERROR
}

case object DatabaseException extends CustomExcecption {
  override val message: String = "database error"
  override val code = Status.INTERNAL_SERVER_ERROR
}
case object DataNotFoundException extends CustomExcecption {
  override val message: String = "data not found"
  override val code = Status.NOT_FOUND
}
